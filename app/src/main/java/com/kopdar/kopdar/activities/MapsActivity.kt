package com.kopdar.kopdar.activities

import android.graphics.*
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment

import com.kopdar.kopdar.R
import com.kopdar.kopdar.service.data.RestaurantL3
import android.support.v4.app.FragmentActivity
import android.os.AsyncTask
import android.support.v4.content.ContextCompat
import android.util.Log
import com.google.android.gms.maps.model.*
import com.kopdar.kopdar.service.DirectionsJSONParser
import com.kopdar.kopdar.service.RestaurantData
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import com.google.android.gms.maps.model.LatLngBounds

class MapsActivity : FragmentActivity(), OnMapReadyCallback {
    private lateinit var restaurant: RestaurantL3

    companion object {
        private lateinit var mMap: GoogleMap
        private const val colorOrigin = "#B65F3D"
        private const val colorDestination = "#7F2805"
        private const val colorLine = "#7F2805"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        restaurant = intent.getParcelableExtra("restaurant")

        val name = findViewById<View>(R.id.name) as TextView
        name.text = restaurant.name

        val address = findViewById<View>(R.id.address) as TextView
        address.text = restaurant.location.address

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Initialize origin & destination points
        var origin: LatLng? = null

        if (RestaurantData.instance.hasOrigin()) {
            origin = LatLng(RestaurantData.instance.lat!!, RestaurantData.instance.lng!!)
            mMap.addMarker(
                MarkerOptions()
                    .icon(getBitmapFromVector(R.drawable.ic_baseline_fiber_manual_record))
                    .anchor(0.5f, 0.5f)
                    .position(origin)
            )
        }

        val destination = LatLng(
            restaurant.location.latitude.toDouble(),
            restaurant.location.longitude.toDouble()
        )

        mMap.addMarker(
            MarkerOptions()
                .position(destination)
                .title(restaurant.name)
        )

        val builder = LatLngBounds.Builder()
        builder.include(destination)

        if (RestaurantData.instance.hasOrigin()) {
            builder.include(origin!!)
        }

        val width = resources.displayMetrics.widthPixels
        val height = resources.displayMetrics.heightPixels
        val padding = (width * 0.25).toInt()
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, padding))

        if (RestaurantData.instance.hasOrigin()) {
            DownloadTask().execute(getDirectionsUrl(origin!!, destination))
        }
    }

    private fun getBitmapFromVector(resId: Int, color: String = colorOrigin): BitmapDescriptor? {
        val drawable = ContextCompat.getDrawable(this, resId)

        if (drawable != null) {
            val bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )

            drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
            drawable.draw(Canvas(bitmap))

            return BitmapDescriptorFactory.fromBitmap(tintBitmap(bitmap, color))
        }

        return null
    }

    private fun tintBitmap(bitmap: Bitmap, color: String): Bitmap {
        val paint = Paint()
        paint.colorFilter = PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_IN)

        val bitmapResult = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmapResult)
        canvas.drawBitmap(bitmap, 0f, 0f, paint)

        return bitmapResult
    }

    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String {
        val strOrigin = "origin=" + origin.latitude + "," + origin.longitude
        val strDest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"
        val mode = "mode=driving"

        // Building the parameters to the web service
        val parameters = "$strOrigin&$strDest&$sensor&$mode"

        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/json?$parameters"
    }

    private class DownloadTask : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg url: String): String {
            return try {
                downloadUrl(url[0])
            } catch (e: Exception) {
                Log.d("Background Task", e.toString())
                ""
            }
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            ParserTask().execute(result)
        }

        @Throws(IOException::class)
        private fun downloadUrl(url: String): String {
            val urlConnection = URL(url).openConnection() as HttpURLConnection
            val input = urlConnection.inputStream
            var data = ""

            try {
                urlConnection.connect()

                val br = BufferedReader(InputStreamReader(input))
                val sb = StringBuffer()

                do {
                    val line = br.readLine()
                    sb.append(line)
                }
                while (line != null)

                data = sb.toString()
                br.close()
            } catch (e: Exception) {
                Log.d("Exception", e.toString())
            } finally {
                input!!.close()
                urlConnection.disconnect()
            }

            return data
        }
    }

    private class ParserTask : AsyncTask<String, Int, List<List<Map<String, String>>>>() {
        override fun doInBackground(vararg jsonData: String): List<List<Map<String, String>>>? {
            return try {
                val obj = JSONObject(jsonData[0])
                DirectionsJSONParser().parse(obj)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        override fun onPostExecute(result: List<List<Map<String, String>>>) {
            val points = mutableListOf<LatLng>()
            val lineOptions = PolylineOptions()

            for (i in result.indices) {
                val path = result[i]

                for (j in path.indices) {
                    val point = path[j]
                    val lat = java.lang.Double.parseDouble(point["lat"])
                    val lng = java.lang.Double.parseDouble(point["lng"])
                    val position = LatLng(lat, lng)

                    points.add(position)
                }

                lineOptions.addAll(points)
                lineOptions.width(12f)
                lineOptions.color(Color.parseColor(colorLine))
                lineOptions.geodesic(true)
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions)
        }
    }
}
