package com.kopdar.kopdar.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.kopdar.kopdar.R
import java.util.*
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.widget.*
import com.kopdar.kopdar.adapter.MenuList
import com.kopdar.kopdar.adapter.ReviewSlider
import com.kopdar.kopdar.service.RestaurantData
import com.kopdar.kopdar.service.data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class RestaurantActivity : AppCompatActivity() {

    private lateinit var restaurant: RestaurantL3
    private lateinit var viewPager: ViewPager
    private lateinit var indicator: TabLayout
    private var userReviews = mutableListOf<Review>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant)

        restaurant = intent.getParcelableExtra("restaurant")

        findViewById<TextView>(R.id.name).text = restaurant.name
        findViewById<TextView>(R.id.address).text = restaurant.location.address
        findViewById<TextView>(R.id.cuisines).text = restaurant.cuisines
        findViewById<RatingBar>(R.id.rating).rating = restaurant.userRating.aggregateRating!!
        findViewById<TextView>(R.id.ratingText).text = restaurant.userRating.ratingText
        findViewById<TextView>(R.id.votes).text = restaurant.userRating.votes.toString()
        findViewById<View>(R.id.btnDirection).setOnClickListener { _ ->
            val intent = Intent(this, MapsActivity::class.java)
            intent.putExtra("restaurant", restaurant)

            startActivity(intent)
        }

        if (restaurant.thumb.isNotEmpty()) {
            val cover = findViewById<View>(R.id.cover) as ImageView
            cover.visibility = View.VISIBLE

            val options = RequestOptions()
                .placeholder(R.drawable.ic_outline_restaurant_menu)
                .error(R.drawable.ic_outline_restaurant_menu)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .transforms(CenterCrop(), RoundedCorners(20))

            Glide.with(this).load(restaurant.thumb).apply(options).into(cover)

            val defaultCover = findViewById<View>(R.id.defaultCover)
            defaultCover.visibility = View.GONE
        }

        // User Reviews
        RestaurantData.instance.getReviews(restaurant.id.toString())
            .enqueue(object: Callback<ReviewResults> {
                override fun onResponse(call: Call<ReviewResults>?, response: Response<ReviewResults>?) {
                    if (this@RestaurantActivity == null) return
                    val body = response?.body()

                    if (body != null && body.resultsShown > 0) {
                        viewPager = findViewById(R.id.viewPager)
                        indicator = findViewById(R.id.indicator)

                        val slider = findViewById<View>(R.id.reviews)
                        slider.visibility = View.VISIBLE

                        for (item in body.results) {
                            userReviews.add(item.review)
                        }

                        viewPager.adapter = ReviewSlider(this@RestaurantActivity, userReviews)
                        indicator.setupWithViewPager(viewPager, true)

                        val timer = Timer()
                        timer.scheduleAtFixedRate(SliderTimer(), 4000, 6000)
                    }
                }

                override fun onFailure(call: Call<ReviewResults>?, t: Throwable?) {
                    t?.printStackTrace()
                }
            })

        // Daily Menus
        RestaurantData.instance.getDailyMenus(restaurant.id.toString())
            .enqueue(object: Callback<DailyMenus> {
                override fun onResponse(call: Call<DailyMenus>?, response: Response<DailyMenus>?) {
                    if (this@RestaurantActivity == null) return
                    val body = response?.body()

                    if (body != null && body.menus!!.isNotEmpty()) {
                        val menus = findViewById<LinearLayout>(R.id.menus)

                        for (menu in body.menus!!) {
                            val dailyMenu = menu.dailyMenu
                            val menuVi = layoutInflater.inflate(R.layout.menu_item, null)
                            val vh = object {
                                lateinit var name: TextView
                                lateinit var startDate: TextView
                            }
                            vh.name = menuVi.findViewById(R.id.name)
                            vh.name.text = dailyMenu.name
                            vh.startDate = menuVi.findViewById(R.id.startDate)
                            vh.startDate.text = SimpleDateFormat("dd MMMM", Locale.US).format(dailyMenu.startDate)

                            // Dishes
                            val dishes = menuVi.findViewById<LinearLayout>(R.id.dishes)

                            for (dishItem in dailyMenu.menus!!) {
                                val dish = dishItem.dish!!
                                val dishVi = layoutInflater.inflate(R.layout.dish_item, null)
                                val dishVh = object {
                                    lateinit var name: TextView
                                    lateinit var price: TextView
                                }

                                dishVh.name = dishVi.findViewById(R.id.name)
                                dishVh.name.text = dish.name
                                dishVh.price = dishVi.findViewById(R.id.price)
                                dishVh.price.text = dish.price

                                dishVi.tag = dishVh
                                dishes.addView(dishVi)
                            }

                            menuVi.tag = vh
                            menus.addView(menuVi)
                        }

                        findViewById<View>(R.id.dailyMenus).visibility = View.VISIBLE
                    }
                }

                override fun onFailure(call: Call<DailyMenus>?, t: Throwable?) {
                    t?.printStackTrace()
                }
            })
    }

    private inner class SliderTimer : TimerTask() {
        override fun run() {
            this@RestaurantActivity.runOnUiThread {
                if (viewPager.currentItem < userReviews.size - 1) {
                    viewPager.currentItem = viewPager.currentItem + 1
                } else {
                    viewPager.currentItem = 0
                }
            }
        }
    }
}
