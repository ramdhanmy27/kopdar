package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class Dish(
    @JsonProperty("dish_id") var id: String? = "",
    @JsonProperty("name") var name: String? = "",
    @JsonProperty("price") var price: String? = ""
): Parcelable
