package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class Photo(
    @JsonProperty("id") var id: String? = "",
    @JsonProperty("url") var url: String? = "",
    @JsonProperty("thumb_url") var thumbUrl: String? = "",
    @JsonProperty("user") var user: User = User(),
    @JsonProperty("res_id") var resId: Int? = 0,
    @JsonProperty("caption") var caption: String? = "",
    @JsonProperty("timestamp") var timestamp: Int? = 0,
    @JsonProperty("friendly_time") var friendlyTime: String? = "",
    @JsonProperty("width") var width: Int? = 0,
    @JsonProperty("height") var height: Int? = 0,
    @JsonProperty("comments_count") var commentsCount: Int? = 0,
    @JsonProperty("likes_count") var likesCount: Int? = 0
): Parcelable