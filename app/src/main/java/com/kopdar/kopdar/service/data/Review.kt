package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class Review(
    @JsonProperty("id") var id: Int? = 0,
    @JsonProperty("rating") var rating: Float? = 0f,
    @JsonProperty("review_text") var reviewText: String? = "",
    @JsonProperty("rating_color") var ratingColor: String? = "",
    @JsonProperty("review_time_friendly") var reviewTimeFriendly: String? = "",
    @JsonProperty("rating_text") var ratingText: String? = "",
    @JsonProperty("timestamp") var timestamp: Int? = 0,
    @JsonProperty("likes") var likes: Int? = 0,
    @JsonProperty("user") var user: User? = User(),
    @JsonProperty("comments_count") var commentsCount: Int? = 0
): Parcelable