package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class DailyMenu(
    @JsonProperty("daily_menu_id") var id: String? = "",
    @JsonProperty("name") var name: String? = "",
    @JsonProperty("dishes") var menus: List<DishItem>? = ArrayList(),
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @JsonProperty("start_date")
    var startDate: Date = Date()
): Parcelable {
    @Parcelize
    class DishItem(@JsonProperty var dish: Dish? = Dish()): Parcelable
}
