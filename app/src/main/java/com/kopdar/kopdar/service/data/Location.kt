package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class Location(
    @JsonProperty("entity_type") var entityType: String? = "",
    @JsonProperty("entity_id") var entityId: Int? = 0,
    @JsonProperty("title") var title: String? = "",
    @JsonProperty("latitude") var latitude: Number? = 0,
    @JsonProperty("longitude") var longitude: Number? = 0,
    @JsonProperty("city_id") var cityId: Int? = 0,
    @JsonProperty("city_name") var cityName: String? = "",
    @JsonProperty("country_id") var countryId: Int? = 0,
    @JsonProperty("country_name") var countryName: String? = ""
): Parcelable