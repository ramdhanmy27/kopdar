package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class Geocode(
    @JsonProperty("locality") var locality: Location = Location(),
    @JsonProperty("popularity") var popularity: Popularity = Popularity(),
    @JsonProperty("link") var link: String? = "",
    @JsonProperty("nearby_restaurants") var nearbyRestaurants: List<RestaurantL3>? = ArrayList()
): Parcelable