package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class SearchResults(
    @JsonProperty("results_found") var resultsFound: Int = 0,
    @JsonProperty("results_start") var resultsStart: Int = 0,
    @JsonProperty("results_shown") var resultsShown: Int = 0,
    @JsonProperty("restaurants") var results: List<RestaurantItem> = ArrayList()
): Parcelable {
    @Parcelize
    class RestaurantItem(@JsonProperty var restaurant: RestaurantL3 = RestaurantL3()): Parcelable
}
