package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class DailyMenus(
    @JsonProperty("status") var status: String? = "success",
    @JsonProperty("daily_menus") var menus: List<DailyMenuItem>? = ArrayList()
//    @JsonProperty("daily_menus") var menus: MutableList<DailyMenuItem>? = mutableListOf()
): Parcelable {
    @Parcelize
    class DailyMenuItem(
        @JsonProperty("daily_menu") var dailyMenu: DailyMenu = DailyMenu()
    ): Parcelable
}
