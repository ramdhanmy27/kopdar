package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class Popularity(
    @JsonProperty("popularity") var popularity: Number? = 0,
    @JsonProperty("nightlife_index") var nightlifeIndex: Number? = 0,
    @JsonProperty("top_cuisines") var topCuisines: List<String>? = ArrayList()
): Parcelable