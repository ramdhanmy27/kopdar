package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class User: Parcelable {
    @JsonProperty("name") var name: String? = ""
    @JsonProperty("zomato_handle") var zomatoHandle: String? = ""
    @JsonProperty("foodie_level") var foodieLevel: String? = ""
    @JsonProperty("foodie_level_num") var foodieLevelNum: Int? = 0
    @JsonProperty("foodie_color") var foodieColor: String? = ""
    @JsonProperty("profile_url") var profileUrl: String? = ""
    @JsonProperty("profile_deeplink") var profileDeeplink: String? = ""
    @JsonProperty("profile_image") var profileImage: String? = ""
}