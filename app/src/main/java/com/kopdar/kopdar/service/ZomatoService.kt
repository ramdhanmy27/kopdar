package com.kopdar.kopdar.service

import com.kopdar.kopdar.service.data.DailyMenus
import com.kopdar.kopdar.service.data.ReviewResults
import com.kopdar.kopdar.service.data.SearchResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ZomatoService {

    @Headers("user-key: d2aaa1bb4c3c026edcf5ef315673aa50")
    @GET("search")
    fun searchNearby(
        @Query("q") query: String?,
        @Query("lat") lat: Double?,
        @Query("lon") lng: Double?,
        @Query("start") start: Long?,
        @Query("count") count: Long?,
        @Query("sort") sortBy: String? = null,
        @Query("order") direction: String? = null
    ): Call<SearchResults>

    @Headers("user-key: d2aaa1bb4c3c026edcf5ef315673aa50")
    @GET("reviews")
    fun getReviews(@Query("res_id") resId: String): Call<ReviewResults>

    @Headers("user-key: d2aaa1bb4c3c026edcf5ef315673aa50")
    @GET("dailymenu")
    fun getDailyMenus(@Query("res_id") resId: String): Call<DailyMenus>
}
