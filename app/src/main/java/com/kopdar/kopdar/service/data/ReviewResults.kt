package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class ReviewResults(
    @JsonProperty("reviews_count") var resultsFound: Int = 0,
    @JsonProperty("reviews_start") var resultsStart: Int = 0,
    @JsonProperty("reviews_shown") var resultsShown: Int = 0,
    @JsonProperty("user_reviews") var results: List<ReviewItem> = ArrayList()
): Parcelable {
    @Parcelize
    class ReviewItem(@JsonProperty var review: Review = Review()): Parcelable
}
