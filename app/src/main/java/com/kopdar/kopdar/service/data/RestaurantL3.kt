package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
data class RestaurantL3(
    @JsonProperty("id") var id: Int = 0,
    @JsonProperty("name") var name: String = "",
    @JsonProperty("url") var url: String = "",
    @JsonProperty("location") var location: ResLocation = ResLocation(),
    @JsonProperty("average_cost_for_two") var averageCostForTwo: Int = 0,
    @JsonProperty("price_range") var priceRange: Int = 0,
    @JsonProperty("currency") var currency: String = "",
    @JsonProperty("thumb") var thumb: String = "",
    @JsonProperty("featured_image") var featuredImage: String = "",
    @JsonProperty("photos_url") var photosUrl: String = "",
    @JsonProperty("menu_url") var menuUrl: String = "",
    @JsonProperty("events_url") var eventsUrl: String = "",
    @JsonProperty("user_rating") var userRating: UserRating = UserRating(),
    @JsonProperty("has_online_delivery") var hasOnlineDelivery: Boolean = false,
    @JsonProperty("is_delivering_now") var isDeliveringNow: Boolean = false,
    @JsonProperty("has_table_booking") var hasTableBooking: Boolean = false,
    @JsonProperty("deeplink") var deeplink: String = "",
    @JsonProperty("cuisines") var cuisines: String = "",
    @JsonProperty("all_reviews_count") var allReviewsCount: Int = 0,
    @JsonProperty("photo_count") var photoCount: Int = 0
//    @JsonProperty("phone_numbers") var phoneNumbers: String = "",
//    @JsonProperty("photos") var photos: List<Photo> = ArrayList(),
//    @JsonProperty("all_reviews") var allReviews: List<Review> = ArrayList()
): Parcelable