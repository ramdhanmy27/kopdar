package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class ResLocation(
    @JsonProperty("address") var address: String = "",
    @JsonProperty("locality") var locality: String = "",
    @JsonProperty("city") var city: String = "",
    @JsonProperty("city_id") var cityId: Int = 0,
    @JsonProperty("latitude") var latitude: Number = 0,
    @JsonProperty("longitude") var longitude: Number = 0,
    @JsonProperty("zipcode") var zipcode: String = "",
    @JsonProperty("country_id") var countryId: Int = 0
): Parcelable