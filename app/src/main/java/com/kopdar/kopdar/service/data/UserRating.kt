package com.kopdar.kopdar.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
class UserRating(
    @JsonProperty("aggregate_rating") var aggregateRating: Float? = 0f,
    @JsonProperty("rating_text") var ratingText: String? = "",
    @JsonProperty("rating_color") var ratingColor: String? = "",
    @JsonProperty("votes") var votes: Int? = 0
): Parcelable