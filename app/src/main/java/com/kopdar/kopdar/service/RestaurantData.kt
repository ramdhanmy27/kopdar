package com.kopdar.kopdar.service

import com.kopdar.kopdar.service.data.DailyMenus
import com.kopdar.kopdar.service.data.ReviewResults
import com.kopdar.kopdar.service.data.SearchResults
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call

class RestaurantData {
    var lat: Double? = null
    var lng: Double? = null

    private lateinit var service: ZomatoService

    companion object {
        val instance: RestaurantData = RestaurantData()
        const val SORT_REAL_DISTANCE = "real_distance"
        const val SORT_RATING = "rating"
        const val SORT_COST = "cost"
        const val DIRECTION_ASC = "asc"
        const val DIRECTION_DESC = "desc"
    }

    var restEndpoint: String = ""
        set(value) {
//            val interceptor = HttpLoggingInterceptor()
//            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
//                .addInterceptor(interceptor)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(value)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client)
                .build()

            service = retrofit.create(ZomatoService::class.java)
        }

    fun fetch(
        search: String? = null,
        start: Long? = null,
        count: Long? = null,
        sortBy: String? = null,
        direction: String? = null
    ): Call<SearchResults> {
        return if (hasOrigin()) {
            service.searchNearby(
                search,
                lat,
                lng,
                start,
                count,
                sortBy ?: SORT_RATING,
                direction ?: DIRECTION_ASC
            )
        }
        else
            service.searchNearby(search, lat, lng, start, count)
    }

    fun getReviews(resId: String): Call<ReviewResults> {
        return service.getReviews(resId)
    }

    fun getDailyMenus(resId: String): Call<DailyMenus> {
        return service.getDailyMenus(resId)
    }

    fun hasOrigin(): Boolean {
        return lat != null && lng != null
    }
}