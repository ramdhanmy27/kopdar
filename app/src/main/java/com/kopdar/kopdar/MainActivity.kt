package com.kopdar.kopdar

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher

import com.kopdar.kopdar.service.RestaurantData
import android.view.View
import com.kopdar.kopdar.service.data.SearchResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.kopdar.kopdar.adapter.PlaceList
import android.widget.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.view.Window
import android.view.WindowManager
import com.kopdar.kopdar.activities.RestaurantActivity
import com.kopdar.kopdar.service.data.RestaurantL3
import kotlinx.android.synthetic.main.place_list.*
import android.widget.AbsListView
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.inputmethod.InputMethodManager
import org.honorato.multistatetogglebutton.MultiStateToggleButton

class MainActivity : AppCompatActivity() {

    private val permissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private lateinit var filter: MultiStateToggleButton
    private val filterOptions = arrayOf("Nearest", "Best")
    private val sortOptions = arrayOf(RestaurantData.SORT_REAL_DISTANCE, RestaurantData.SORT_RATING)
    private var sortBy = sortOptions[1]
    private var direction = RestaurantData.DIRECTION_DESC

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.place_list)

        // API Client Service
        RestaurantData.instance.restEndpoint = resources.getString(R.string.zomato_rest_endpoint)

        if (!hasPermission(permissions)) {
            Toast.makeText(this, "Kopdar cannot access your GPS", Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(this, permissions, 1337)
        }
        else {
            val loc = getCurrentLocation()

            if (loc != null) {
                RestaurantData.instance.lat = loc.latitude
                RestaurantData.instance.lng = loc.longitude
            }
        }

        // Search Restaurants Nearby
        val inputSearch = findViewById<View>(R.id.inputSearch) as EditText

        inputSearch.setOnEditorActionListener(
            TextView.OnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard(this@MainActivity)
                    showNearby(inputSearch.text.toString())
                    return@OnEditorActionListener true
                }

                false
            }
        )

        inputSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                if (editable.toString().isNotEmpty()) {
                    clearSearch.visibility = View.VISIBLE
                }
                else {
                    clearSearch.visibility = View.GONE
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        // Clear SearchResults
        val clearSearch = findViewById<View>(R.id.clearSearch)
        clearSearch.setOnClickListener { _ ->
            inputSearch.setText(null, TextView.BufferType.EDITABLE)
            hideKeyboard(this@MainActivity)
            showNearby()
        }

        filter = findViewById(R.id.filter)

        if (RestaurantData.instance.hasOrigin()) {
            filter.setElements(filterOptions)
            filter.enableMultipleChoice(false)
            filter.states = booleanArrayOf(false, true)
            filter.setOnValueChangedListener { pos ->
                sortBy = sortOptions[pos]
                direction = if (sortBy == RestaurantData.SORT_RATING)
                    RestaurantData.DIRECTION_DESC
                else
                    RestaurantData.DIRECTION_ASC

                showNearby(inputSearch.text.toString())
            }
        }
        else filter.visibility = View.GONE

        // Refresh Button
        val btnRefresh = findViewById<View>(R.id.btnRefresh) as Button
        btnRefresh.setOnClickListener { _ ->
            showNearby(inputSearch.text.toString())
        }

        // Initialization
        showNearby(inputSearch.text.toString())
    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation(): Location? {
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)
    }

    private fun hasPermission(perms: Array<String>): Boolean {
        for (perm in perms) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, perm)) {
                return false
            }
        }

        return true
    }

    private fun hideKeyboard(activity: Activity) {
        val view = activity.findViewById<View>(android.R.id.content)

        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun showNearby(query: String = "") {
        val loading = findViewById<View>(R.id.loading)
        val errorConnection = findViewById<View>(R.id.errorConnection)
        val emptyResult = findViewById<View>(R.id.emptyResult)
        val placeList = findViewById<ListView>(android.R.id.list)

        loading.visibility = View.VISIBLE
        placeList.visibility = View.GONE
        errorConnection.visibility = View.GONE
        emptyResult.visibility = View.GONE

        val call: Call<SearchResults> = RestaurantData.instance.fetch(query, null, null, sortBy, direction)

        call.enqueue(object: Callback<SearchResults> {
            override fun onResponse(call: Call<SearchResults>?, response: Response<SearchResults>?) {
                if (this@MainActivity == null) return

                val results: SearchResults? = response?.body()
                loading.visibility = View.GONE

                if (results != null && results.resultsShown > 0) {
                    placeList.adapter = PlaceList(this@MainActivity, results)
                    placeList.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                        val restaurant: RestaurantL3 = results.results[position].restaurant
                        val intent = Intent(this@MainActivity, RestaurantActivity::class.java)
                        intent.putExtra("restaurant", restaurant)

                        startActivity(intent)
                    }

                    placeList.setOnScrollListener(object: AbsListView.OnScrollListener {
                        override fun onScroll(view: AbsListView, offset: Int, visible: Int, total: Int) {
                            if (offset > 0 && (offset + visible) == total && total != 0) {
                                if (loading.visibility == View.GONE) {
//                                    val q = Math.random().toString()
//                                    inputSearch.setText(q, TextView.BufferType.EDITABLE)
//                                    showNearby(q) // TODO: Load More
                                }
                            }
                        }

                        override fun onScrollStateChanged(p0: AbsListView?, p1: Int) {}
                    })

                    placeList.visibility = View.VISIBLE
                }
                else {
                    emptyResult.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<SearchResults>?, t: Throwable?) {
                loading.visibility = View.GONE
                errorConnection.visibility = View.VISIBLE
            }
        })
    }
}
