package com.kopdar.kopdar.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.support.v4.view.PagerAdapter
import android.view.View
import android.widget.RatingBar
import com.kopdar.kopdar.R
import com.kopdar.kopdar.service.data.Review

class ReviewSlider(
    private val context: Context,
    private val review: MutableList<Review>
) : PagerAdapter() {

    override fun getCount(): Int {
        return review.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    @SuppressLint("SetTextI18n")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.slide_item, null)

        view.findViewById<TextView>(R.id.reviewText).text = review[position].reviewText
        view.findViewById<TextView>(R.id.user).text = "-"+ review[position].user?.name
        view.findViewById<RatingBar>(R.id.rating).rating = review[position].rating!!

//        val linearLayout = view.findViewById(R.id.linearLayout) as LinearLayout
//        linearLayout.setBackgroundColor(color[position])

        val viewPager = container as ViewPager
        viewPager.addView(view, 0)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        val viewPager = container as ViewPager
        viewPager.removeView(obj as View)
    }
}