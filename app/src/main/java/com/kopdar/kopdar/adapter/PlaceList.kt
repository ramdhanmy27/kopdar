package com.kopdar.kopdar.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RatingBar
import com.kopdar.kopdar.service.data.SearchResults
import android.widget.TextView
import com.kopdar.kopdar.R
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.model.LatLng
import com.kopdar.kopdar.service.RestaurantData
import java.text.DecimalFormat
import java.util.*


class PlaceList(private val context: Activity, private val results: SearchResults?) : BaseAdapter() {

    class ViewHolder {
        internal lateinit var address: TextView
        internal lateinit var name: TextView
        internal lateinit var cuisines: TextView
        internal lateinit var thumbnail: ImageView
        internal lateinit var rating: RatingBar
        internal lateinit var distance: TextView
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, view: View?, parent: ViewGroup?): View? {
        var row = view
        val inflater: LayoutInflater = context.layoutInflater
        val vh: ViewHolder

        if (view == null) {
            row = inflater.inflate(R.layout.place_item, null, true)

            vh = ViewHolder()
            vh.name = row.findViewById(R.id.name)
            vh.address = row.findViewById(R.id.address)
            vh.cuisines = row.findViewById(R.id.cuisines)
            vh.thumbnail = row.findViewById(R.id.thumbnail)
            vh.rating = row.findViewById(R.id.rating)
            vh.distance = row.findViewById(R.id.distance)

            row.tag = vh
        }
        else {
            vh = view.tag as ViewHolder
        }

        if (row != null && results?.results!!.isNotEmpty()) {
            val restaurant = results.results[position].restaurant
            vh.name.text = restaurant.name
            vh.address.text = restaurant.location.address
            vh.cuisines.text = restaurant.cuisines
            vh.rating.rating = restaurant.userRating.aggregateRating!!

            // distance
            if (RestaurantData.instance.hasOrigin()) {
                val distance = Math.round(measureDistance(
                    LatLng(RestaurantData.instance.lat!!, RestaurantData.instance.lng!!),
                    LatLng(restaurant.location.latitude as Double, restaurant.location.longitude as Double)
                ) * 1000)

                vh.distance.text = "~"+ format(distance)
            }

            val options = RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .transforms(CenterCrop(), RoundedCorners(20))

            Glide.with(row.context)
                .load(restaurant.thumb)
                .apply(options)
                .into(vh.thumbnail)
        }

        return row
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return results?.resultsShown!!
    }

    fun measureDistance(StartP: LatLng, EndP: LatLng): Double {
        val radius = 6371 //radius of earth in Km
        val lat1 = StartP.latitude
        val lat2 = EndP.latitude
        val lon1 = StartP.longitude
        val lon2 = EndP.longitude
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lon2 - lon1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2)
        val c = 2 * Math.asin(Math.sqrt(a))

        return radius * c // kilometers
    }

    private fun format(value: Long): String {
        return if (value >= 1000) {
            (Math.round((value / 1000) * 10.0) / 10).toString() + "km"
        }
        else {
            value.toString() + "m"
        }
    }
}