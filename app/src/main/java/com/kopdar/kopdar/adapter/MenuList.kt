package com.kopdar.kopdar.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.kopdar.kopdar.service.data.DailyMenus
import android.widget.TextView
import com.kopdar.kopdar.R
import java.text.SimpleDateFormat
import java.util.*


class MenuList(private val context: Activity, private val dailyMenus: DailyMenus) : BaseAdapter() {

    class ViewHolder {
        internal lateinit var name: TextView
        internal lateinit var startDate: TextView
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup?): View? {
        var row = view
        val inflater = context.layoutInflater
        val vh: ViewHolder

        if (view == null) {
            row = inflater.inflate(R.layout.menu_item, null, true)

            vh = ViewHolder()
            vh.name = row.findViewById(R.id.name)
            vh.startDate = row.findViewById(R.id.startDate)

            row.tag = vh
        }
        else {
            vh = view.tag as ViewHolder
        }

        if (row != null && dailyMenus.menus!!.isNotEmpty()) {
            val menu = dailyMenus.menus!![position].dailyMenu
            vh.name.text = menu.name
            vh.startDate.text = SimpleDateFormat("dd MMMM", Locale.US).format(menu.startDate)
        }

        return row
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dailyMenus.menus!!.size
    }
}